// NextJs Config;
const path = require('path');
const withLess = require('@zeit/next-less');
const withImages = require('next-images');

module.exports = withImages(withLess({
  webpack: config => {
    config.resolve.alias['components'] = path.resolve(__dirname, './components');
    config.resolve.alias['media'] = path.resolve(__dirname, './media');

    config.module.rules.push({
      test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
      use: {
        loader: 'url-loader',
        options: {
          limit: 100000,
          publicPath: '/_next/static/'
        }
      }
    });

    return config;
  },
  exportTrailingSlash: true,
  exportPathMap: function() {
    return {
      '/': { page: '/' }
    };
  },
  cssModules: true,
  cssLoaderOptions: {
    importLoaders: 1,
    localIdentName: "[local]___[hash:base64:5]",
  }
}));
