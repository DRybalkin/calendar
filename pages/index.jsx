import React from 'react';

const Home = () => (
  <style jsx global>{`
    html {
        font-family: 'Roboto', sans-serif;
      }

     body {
      margin: 0;
      padding: 0;
     }
  `}
  </style>
);

export default Home;
