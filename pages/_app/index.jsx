import React from 'react';
import App from 'next/app';

// Components
import Layout from '../../components/Layout';

class RootApp extends App {
  render() {
    const { Component, pageProps } = this.props;
    return (
      <Layout>
        <Component {...pageProps} />
      </Layout>
    )
  }
}

export default RootApp;
