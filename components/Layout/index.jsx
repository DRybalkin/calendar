import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Components
import Gallery from 'react-photo-gallery';
import { Modal, ModalGateway } from 'react-images';
import { PopupContent, Footer } from 'components';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

import { photos } from "./photos";
import { data } from "./data";

class Layout extends Component {
  state = {
    currentImage: null,
  };

  toggleModal = () => {
    this.setState(state => ({ currentImage: null }));
  };

  onClick = (_, obj) => {
    this.setState(state => ({ currentImage: obj.index }));
  };

  render () {
    const { children } = this.props;
    const { currentImage } = this.state;

    return (
      <React.Fragment>
        <div className={cx('container')}>
          <div className={cx('heading')}>
            <h1
              className={cx('title')}
              dangerouslySetInnerHTML={{ __html: data.title }}
            />
            <div
              className={cx('description')}
              dangerouslySetInnerHTML={{ __html: data.description }}
            />
          </div>

          <div className={cx('calendar')}>
            <Gallery
              photos={photos}
              direction={"column"}
              columns={5}
              onClick={this.onClick}
              margin={5}
            />
          </div>
          <ModalGateway>
            {(currentImage !== null) && (
              <Modal onClose={this.toggleModal}>
                <PopupContent
                  popupData={photos[currentImage]}
                />
              </Modal>
            )}
          </ModalGateway>
        </div>

        <Footer />
        {children}
      </React.Fragment>
    );
  }
}

Layout.propTypes = {
  children: PropTypes.object,
};

export default Layout;
