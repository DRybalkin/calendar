export const photos = [
  {
    src: "/static/1.png",
    width: 4,
    height: 3,
    date: 1,
    title: '1 марта',
    weekend: false,
    description: ['Открываем зиму официально и выходим на финишную прямую этого года! Вас ждет масса крутых мероприятий в декабре! Полетели!'
    ]
  },
  {
    src: "/static/2.png",
    width: 3,
    height: 2,
    date: 2,
    title: '2 декабря',
    weekend: true,
    description: [
    ]
  },
  {
    src: "/static/3.png",
    width: 4,
    height: 3,
    date: 3,
    title: '3 декабря',
    weekend: true,
    description: [
    ]
  },
  {
    src: "/static/4.png",
    width: 2,
    height: 2,
    date: 4,
    title: '4 декабря',
    weekend: true,
    description: ['В моменты стресса наш организм включает турборежим – реагирует тело, чувства и разум. Но в таком состоянии долго жить не получится – расходуются все ресурсы с трехкратной силой. Важно выдохнуть и от мыслей перейти к действиям – поработать над внутренними ощущениями. Если ты чувствуешь, что нуждаешься в поддержке и хочешь обсудить свою ситуацию – <a target="_blank" href="https://bu-online.beeline.ru/view_doc.html?mode=poll&object_id=7071602479931611663">запишись</a> на индивидуальную консультацию к нашим психологам.'
    ]
  },
  {
    src: "/static/5.png",
    width: 2,
    height: 3,
    date: 5,
    title: '5 декабря',
    weekend: true,
    description: [
    ]
  },
  {
    src: "/static/7.png",
    width: 3,
    height: 4,
    date: 7,
    title: '7 декабря',
    weekend: true,
    description: ['Время строить карьеру! В этом тебе поможет наш новый проект: "Моя карьера".<br>Его цель - познакомить тебя с отделами и департаментами, показать перспективы роста и возможности продвижения по карьерной лестнице.<br>В Личном Кабинете ты можешь построить карьерный трек в компании до желаемой должности , познакомиться с функционалом и задачами интересующих тебя направлений. Переходи по <a target="_blank" href="https://space.beeline.ru/Regions/CPK/Pages/my_career.aspx">ссылке</a>, используй возможности.'
    ]
  },
  {
    src: "/static/6.png",
    width: 3,
    height: 2,
    date: 6,
    title: '6 декабря',
    weekend: true,
    description: [
    ]
  },
  {
    src: "/static/8.png",
    width: 2,
    height: 2,
    date: 8,
    title: '8 декабря',
    weekend: true,
    description: ['Наши корпоративные награды пополнились еще одной важной номинацией - "Спасибо за работу" от руководителей ЦПК и ЦТМ. Вручать награды лучшим сотрудникам руководители ЦПК будут раз в квартал. Уже на этой неделе их получат наши красавчики по итогу 4 квартала 2022 года'
    ]
  },
  {
    src: "/static/9.png",
    width: 2,
    height: 3,
    date: 9,
    title: '9 декабря',
    weekend: false,
    description: [
    ]
  },
  {
    src: "/static/11.png",
    width: 2,
    height: 2,
    date: 11,
    title: '11 декабря',
    weekend: false,
    description: [
    ]
  },
  {
    src: "/static/10.png",
    width: 2,
    height: 2,
    date: 10,
    title: '10 декабря',
    weekend: false,
    description: ['Мы стартуем с нашими новогодними мероприятиями именно сегодня! Тебя ждет много всего, просто будь активным и встречай новый год с нами!'
    ]
  },
  {
    src: "/static/13.png",
    width: 1,
    height: 2,
    date: 13,
    title: '13 декабря',
    weekend: false,
    description: ['Ноябрь - это предвкушение нового года, но мы провели его круто и ярко! Читай Дайджест, ты будешь знать про жизнь наших ЦПК все! И не забывай, что его можно смотреть, включайся!'
    ]
  },
  {
    src: "/static/12.png",
    width: 4,
    height: 3,
    date: 12,
    title: '12 декабря',
    weekend: false,
    description: ['С 12 по 16 декабря предлагаем вашим детям превратиться в творцов и изобразить Новый год с нашими главными героями - апперами! Можно рисовать, лепить, шить, клеить и вырезать! Если вашему ребенку от 3 до 14 лет - вперед, к творчеству! Работы принимаем в телеграмм, отправлять Татьяне Кошкиной. Главное - не забыть подписать: ФИО родителя, город (если ДКЦ - укажите), ФИ ребенка, возраст. Если в конкурсе участвуют ваши дети, ждем работу от каждого!'
    ]
  },
  {
    src: "/static/16.png",
    width: 3,
    height: 3,
    date: 16,
    title: '16 декабря',
    weekend: false,
    description: [
    ]
  },
  {
    src: "/static/14.png",
    width: 4,
    height: 3,
    date: 14,
    title: '14 декабря',
    weekend: false,
    description: ['Хочешь получать скидки и экономить деньги? Тогда тебе <a target="_blank" href="https://beeline.spotygo.ru/">сюда</a>!'  
    ]
  },
  {
    src: "/static/15.png",
    width: 3,
    height: 2,
    date: 15,
    title: '15 декабря',
    weekend: true,
    description: ['Зима не за горами, а мы спешим поделиться с вами главными новостями октября, который был теплым и уютным, потому что мы - вместе! Напоминаем, что наш Дайджест можно не только читать, но и смотреть!  Ты будешь знать всё, точно тебе говорим! Все про бизнес, события, жизнь ЦПК, про конкурсы и праздники. В общем, не будем долго рассказывать, просто читай и будь в курсе!'
    ]
  },
  {
    src: "/static/17.png",
    width: 4,
    height: 3,
    date: 17,
    title: '17 декабря',
    weekend: false,
    description: [
    ]
  },
  {
    src: "/static/19.png",
    width: 3,
    height: 3,
    date: 19,
    title: '19 декабря',
    weekend: true,
    description: ['Если работаешь дома, то, однозначно, нужно украсить домашнее рабочее место! Если ты - сотрудник ДКЦ - этот конкурс для тебя! Украшай рабочее место и присылай фото на почту digest@beeline.local, призы найдут тебя!'
    ]
  },
  {
    src: "/static/18.png",
    width: 3,
    height: 2,
    date: 18,
    title: '18 декабря',
    weekend: false,
    description: [
    ]
  },
  {
    src: "/static/20.png",
    width: 2,
    height: 2,
    date: 20,
    title: '20 декабря',
    weekend: true,
    description: ['Это будет круто! Проверим ваши знания и сыграем в квиз! Собирай команду, скоро расскажем, как зарегистрироваться! Подарки обещаем классные, тебе понравится! Зарядимся новогодним настроением на все 100%!'
    ]
  },
  {
    src: "/static/21.png",
    width: 2,
    height: 3,
    date: 21,
    title: '21 декабря',
    weekend: true,
    description: [
    ]
  },
  {
    src: "/static/22.png",
    width: 2,
    height: 2,
    date: 22,
    title: '22 декабря',
    weekend: true,
    description: [
    ]
  },
  {
    src: "/static/23.png",
    width: 2,
    height: 2,
    date: 23,
    title: '23 декабря',
    weekend: false,
    description: ['Пятница – время новогоднего веселья!<br>Устроим новогодний флешмоб, закупим новогодние аксессуары и по старой доброй традиции – мандарины всем!<br>Фото с этого дня выложим в Pro_Service и похвастаемся, кто больше всего съел витамина C!'
    ]
  },
  {
    src: "/static/24.png",
    width: 4,
    height: 3,
    date: 24,
    title: '24 декабря',
    weekend: false,
    description: [
    ]
  },
  {
    src: "/static/25.png",
    width: 4,
    height: 3,
    date: 25,
    title: '25 декабря',
    weekend: true,
    description: ['Регистрируйтесь на новую встречу экосообщества компании – мы обязательно пришлем все детали. Регистрация по ссылке <a target="_blank" href="https://internal.beeline.ru/wheretogo/app/beegreen2502/">перейти</a>'
    ]
  },
  {
    src: "/static/28.png",
    width: 2,
    height: 2.03,
    date: 28,
    title: '28 декабря',
    weekend: false,
    description: [
    ]
  },
  {
    src: "/static/30.png",
    width: 2,
    height: 1.695,
    date: 30-31,
    title: '30-31 декабря',
    weekend: false,
    description: ['С новым 2023 годом, коллеги! Ураааа!!!'
    ]
  },
  {
    src: "/static/29.png",
    width: 4,
    height: 3.39,
    date: 29,
    title: '29 декабря',
    weekend: false,
    description: ['Читать полезно, точно тебе говорим. Билайн заботится о своих сотрудниках и абсолютно бесплатно дает доступ в электронные библиотеки: Альпина, Миф и Периодика. Лови <a target="_blank" href="https://space.beeline.ru/BEELINE_University/Pages/Page.aspx">ccskre</a> и читай!'
    ]
  },
  {
    src: "/static/27.png",
    width: 4,
    height: 3.06,
    date: 27,
    title: '27 декабря',
    weekend: false,
    description: []
  },
  {
    src: "/static/26.png",
    width: 2,
    height: 1.2,
    date: 26,
    title: '26 декабря',
    weekend: true,
    description: [
    ]
  }
];
