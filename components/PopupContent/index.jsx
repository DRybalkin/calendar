import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Components
import { Picture } from 'components';

import { colors } from './colors';
import { wishes } from './wishes';
import { emoji } from './emoji';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

class PopupContent extends Component {
  randomInteger = (min, max) => {
    let rand = min - 0.5 + Math.random() * (max - min + 1);
    return Math.round(rand);
  };

  renderRegularContent = () => {
    const { popupData } = this.props;

    const newDate = new Date();
    const day = newDate.getDate();

    const clrIndexWishes = this.randomInteger(0, wishes.length - 1);
    const clrIndexEmoji = this.randomInteger(0, emoji.length - 1);

    if (popupData.date > day && !popupData.weekend) {
      return (
        <React.Fragment>
          <div className={cx('gif')}>
            <Picture src="/static/image/giphy.gif" />
          </div>
          <div
            className={cx('description')}
            dangerouslySetInnerHTML={{ __html: `А ${popupData.date} числа отличный день, чтобы побеждать!` }}
          />
        </React.Fragment>
      )
    }

    if (!popupData.description) {
      return (
        <React.Fragment>
          <div className={cx('emoji')}>
            <Picture src={emoji[clrIndexEmoji]} />
          </div>
          <div
            className={cx('description')}
            dangerouslySetInnerHTML={{ __html: wishes[clrIndexWishes] }}
          />
        </React.Fragment>
      );
    }

    return (
      <ul className={cx('descriptionList', (popupData.description.length <= 1) && 'listStyleNone')}>
        {popupData.description.map((item, index) => (
          <li key={index}>
            <div dangerouslySetInnerHTML={{ __html: item }} />
          </li>
        ))}
      </ul>
    );
  };

  render() {
    const { popupData } = this.props;
    const clrIndex = this.randomInteger(0, colors.length - 1);

    return (
      <div className={cx('simpleGallery')}>
        <div className={cx('container')} style={{ background: colors[clrIndex] }}>
          <div
            className={cx('date')}
            dangerouslySetInnerHTML={{ __html: popupData.title }}
          />
          {this.renderRegularContent()}
        </div>
        {/**popupData.date === 9 && (
          <Picture src="/static/image/salute2.gif" className={cx('animation')} />
        )*/}
      </div>
    );
  }
}

PopupContent.propTypes = {
  popupData: PropTypes.shape({
    title: PropTypes.string,
    date: PropTypes.number,
  }),
};

export default PopupContent;
