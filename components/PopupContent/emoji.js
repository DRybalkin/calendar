export const emoji = [
  '/static/emoji/angel.svg',
  '/static/emoji/cap.svg',
  '/static/emoji/greek.svg',
  '/static/emoji/happy.svg',
  '/static/emoji/laughing.svg',
  '/static/emoji/love.svg',
  '/static/emoji/money.svg',
  '/static/emoji/mustache.svg',
  '/static/emoji/robot.svg',
  '/static/emoji/star.svg',
  '/static/emoji/sunglass_2.svg',
  '/static/emoji/sunglass_3.svg'
];
