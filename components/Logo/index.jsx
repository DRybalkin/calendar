import React from 'react';
import PropTypes from 'prop-types';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

const Logo = ({ className, name }) => (
  <div className={cx('logo', className, name)} />
);

Logo.propTypes = {
  className: PropTypes.string,
  name: PropTypes.string,
};

export default Logo;
